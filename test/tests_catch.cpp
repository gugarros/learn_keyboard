#include "catch.hpp"
#include "../src/Word.hpp"
#include "../src/Controller.hpp"

#include <codecvt>

TEST_CASE("Creation Word")
{
    Word mot("couleur");
    CHECK("couleur" == mot.getWord());
    CHECK(7 == mot.getNbLettres());

    Word mot2;
    CHECK("" == mot2.getWord());
    CHECK(0 == mot2.getNbLettres());

    Word mot3("â");
    CHECK("â" == mot3.getWord());
    CHECK(1 == mot3.getNbLettres());

    Word mot4("^");
    CHECK("^" == mot4.getWord());
    CHECK(1 == mot4.getNbLettres());

    Word mot5("¨");
    CHECK("¨" == mot5.getWord());
    CHECK(1 == mot5.getNbLettres());

    Word mot6("accélérèrent");
    CHECK("accélérèrent" == mot6.getWord());
    CHECK(12 == mot6.getNbLettres());

    std::string a;
    std::vector<std::string> v2 = {"à","á","â","ç","è","é","ê","ë","î","ï","ô","ö","ù","û","ü","¨"};
    for (unsigned long int i = 0; i<v2.size();++i){
        a = v2[i];
        std::cout << a <<"|"<< a.length() <<"|"<< (int) a[0]<< "|"<<(int) a[1]<<std::endl;
    }
    std::vector<std::string>
    v1 = {"^",",",";",":","!","?",".","/","\"","'","(",")","{","}","[","]","-","+","="," "};
    for (unsigned long int i = 0; i<v1.size();++i){
        a = v1[i];
        std::cout << a <<"|"<< a.length() <<"|"<< (int) a[0]<<std::endl;
    }
}

TEST_CASE("Creation controller")
{
    Controller controller;
    CHECK(336531 == controller.getNbMotsTot());
}

TEST_CASE("Creation controller lettre normale")
{
    Controller controller("w");
    CHECK(537 == controller.getNbMotsTot());
}

TEST_CASE("Creation controller lettre bizarre")
{
    Controller controller("î");
    CHECK(3172 == controller.getNbMotsTot());
}

TEST_CASE("Creation controller lettres normales")
{
    Controller controller("wzial");
    CHECK(5 == controller.getNbMotsTot());
    // controller.print_words();
}

TEST_CASE("Creation controller lettres bizarres")
{
    Controller controller("éèâ");
    CHECK(7 == controller.getNbMotsTot());
    // controller.print_words();
}
TEST_CASE("Creation controller motif complexe")
{
    Controller controller("gabrièllllllle");//je sais que c'est faux mais je veux test
    CHECK(3 == controller.getNbMotsTot());
    controller.print_words();
}
TEST_CASE("Creation controller . union")
{
    Controller controller(".wx");
    CHECK(9085 == controller.getNbMotsTot());
}

TEST_CASE("Creation controller . union compliqué")
{
    Controller controller(".byc");
    CHECK(144241 == controller.getNbMotsTot());
}

TEST_CASE("Creation controller ¨")
{
    Controller controller("¨");
    CHECK(1047 == controller.getNbMotsTot());
}

TEST_CASE("Creation controller ^")
{
    Controller controller("^");
    CHECK(28116 == controller.getNbMotsTot());
}

// TEST_CASE("Creation controller plusieurs lettres simple")
// {
//     // Controller controller("ab");

//     // CHECK(3172 == controller.getNbMotsTot());
//     std::u16string oui = u"âbc";
  
//     std::wstring_convert<std::codecvt_utf8<char16_t>, char16_t> converter;
//     std::cout << converter.to_bytes(oui[0]) << std::endl;
//     CHECK(1 == 1);
    
// }


TEST_CASE("Typing insertion")
{
    Typing typing("dragon");
    CHECK("dragon" == typing.getWord());
    CHECK(6 == typing.getNbLettres());

    typing.addLetter("n");
    CHECK("dragonn" == typing.getWord());
    CHECK(7 == typing.getNbLettres());

    typing.addLetter("e");
    CHECK("dragonne" == typing.getWord());
    CHECK(8 == typing.getNbLettres());

    typing.addLetter("s");
    CHECK("dragonnes" == typing.getWord());
    CHECK(9 == typing.getNbLettres());
}

TEST_CASE("Typing insertion bizarre")
{
    Typing typing("dragon");
    CHECK("dragon" == typing.getWord());
    CHECK(6 == typing.getNbLettres());

    typing.addLetter("n");
    CHECK("dragonn" == typing.getWord());
    CHECK(7 == typing.getNbLettres());

    typing.addLetter("é");
    CHECK("dragonné" == typing.getWord());
    CHECK(8 == typing.getNbLettres());

    typing.addLetter("e");
    CHECK("dragonnée" == typing.getWord());
    CHECK(9 == typing.getNbLettres());

    typing.addLetter("s");
    CHECK("dragonnées" == typing.getWord());
    CHECK(10 == typing.getNbLettres());
}

TEST_CASE("Typing suppression")
{
    Typing typing("dragons");
    CHECK("dragons" == typing.getWord());
    CHECK(7 == typing.getNbLettres());

    typing.supprLetter();
    CHECK("dragon" == typing.getWord());
    CHECK(6 == typing.getNbLettres());

    typing.supprLetter();
    CHECK("drago" == typing.getWord());
    CHECK(5 == typing.getNbLettres());
}

TEST_CASE("Typing suppression bizarre")
{
    Typing typing("dragonnées");
    CHECK("dragonnées" == typing.getWord());
    CHECK(10 == typing.getNbLettres());

    typing.supprLetter();
    CHECK("dragonnée" == typing.getWord());
    CHECK(9 == typing.getNbLettres());

    typing.supprLetter();
    CHECK("dragonné" == typing.getWord());
    CHECK(8 == typing.getNbLettres());

    typing.supprLetter();
    CHECK("dragonn" == typing.getWord());
    CHECK(7 == typing.getNbLettres());

    typing.supprLetter();
    CHECK("dragon" == typing.getWord());
    CHECK(6 == typing.getNbLettres());
}

TEST_CASE("Typing getLast simple")
{
    Typing typing("dragon");
    CHECK("n" == typing.getLast());
    Typing typing2("dragon^");
    CHECK("^" == typing2.getLast());
}


TEST_CASE("Typing getLast bizarre")
{
    Typing typing("dragon¨");
    CHECK("¨" == typing.getLast());
    Typing typing2("dragonné");
    CHECK("é" == typing2.getLast());
}


TEST_CASE("Creation controller + ajout de lettres")
{
    Controller controller("wzial");
    CHECK(5 == controller.getNbMotsTot());
    controller.print_words();
    
    controller.pressLetter('c');
    CHECK("c" == controller.getTyping(0).getWord());
    controller.pressLetter('r');
    CHECK("cr" == controller.getTyping(0).getWord());
    controller.pressDel();
    CHECK("c" == controller.getTyping(0).getWord());
    controller.pressDel();
    CHECK("" == controller.getTyping(0).getWord());
    // controller.pressDel();
    controller.pressLetter('c');
    controller.pressLetter('r');
    controller.pressSpace();
    CHECK("cr" == controller.getTyping(0).getWord());
    CHECK("" == controller.getTyping(1).getWord());
    //ca a l'air de bien marcher
}

TEST_CASE("Comparaison typing/word simple")
{
    Word word1 = Word("words");
    Typing typing1 = Typing("words");
    CHECK(0 == is_same(word1,typing1));

    Word word2 = Word("wordsss");
    Typing typing2 = Typing("words");
    CHECK(0 == is_same(word2,typing2));

    Word word3 = Word("words");
    Typing typing3 = Typing("wordsss");
    CHECK(0 != is_same(word3,typing3));

    Word word4 = Word("");
    Typing typing4 = Typing("");
    CHECK(0 == is_same(word4,typing4));

    Word word5 = Word("a");
    Typing typing5 = Typing("");
    CHECK(0 == is_same(word5,typing5));

    Word word6 = Word("a");
    Typing typing6 = Typing("o");
    CHECK(0 != is_same(word6,typing6));
}

TEST_CASE("Comparaison typing/word bizarre")
{
    Word word1 = Word("abcdéfg");
    Typing typing1 = Typing("abcdéfg");
    CHECK(0 == is_same(word1,typing1));

    Word word2 = Word("âbcdéfg");
    Typing typing2 = Typing("âbcdé");
    CHECK(0 == is_same(word2,typing2));
}

TEST_CASE("Comparaison typing/word ^")
{
    Word word1 = Word("maître");
    Typing typing1 = Typing("ma^");
    CHECK(0 == is_same(word1,typing1));

    Word word2 = Word("maître");
    Typing typing2 = Typing("mo^");
    CHECK(0 != is_same(word2,typing2));
}

TEST_CASE("Comparaison typing/word ¨")
{
    Word word1 = Word("Noël");
    Typing typing1 = Typing("No¨");
    CHECK(0 == is_same(word1,typing1));

    Word word2 = Word("Noël");
    Typing typing2 = Typing("Na¨");
    CHECK(0 != is_same(word2,typing2));
}



// TEST_CASE("TEST VECTEUR")
// {
//     std::vector<int> test;
//     test.push_back(2);
//     CHECK(test[0] == 2);
// }
// Controller controller("amoureusegn");
// CHECK(114 == controller.getNbMotsTot());
// controller.print_words();