#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <algorithm>
#include <random>
#include <vector>
#include <list>
#include <chrono>
//https://thenumbat.github.io/cpp-course/sdl2/08/08.html
#include "Dessin.hpp"
#include "Controller.hpp"
#include "Time.hpp"

//et un mode pour mettre la première lettre du mot en majuscule
//faire une classe de mots word
//avec pour chaque mot un attribut nb words juste / faux ou directement dans le controller
//nb char juste/faux
//nb tot_car juste
//il faut faire un enumerate dans dessin.cpp/hpp pour HAUT BAS GAUCHE DROITE



int main(int argc, char** argv){

    int largeur =  1200; //1440
    int hauteur = 800;
    SDL_Window *window = NULL;               // Future fenêtre
    /* Initialisation de la SDL  + gestion de l'échec possible */
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
    SDL_Log("Error : SDL initialisation - %s\n", SDL_GetError());      // l'initialisation de la SDL a échoué 
    exit(EXIT_FAILURE);
    }

    /* Création de la fenêtre */
    window = SDL_CreateWindow(
        "Apprend a taper",                    // codage en utf8, donc accents possibles
        SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,                                  // coin haut gauche en haut gauche de l'écran
        largeur, hauteur,                              // largeur , hauteur
        SDL_WINDOW_RESIZABLE);                 // redimensionnable
    if (window == NULL) {
    SDL_Log("Error : SDL window 1 creation - %s\n", SDL_GetError());   // échec de la création de la fenêtre
    SDL_Quit();
    exit(EXIT_FAILURE);
    }

    if (TTF_Init() != 0) fprintf(stderr, "Erreur d'initialisation TTF : %s\n", TTF_GetError()); 

    TTF_Font * font = NULL;
    font = TTF_OpenFont("../res/font/arial/arial_unicode_ms/ArialUnicodeMS.ttf", 40 );
    //TTF_SetFontStyle(font, TTF_STYLE_ITALIC | TTF_STYLE_BOLD);           // en italique, gras


    int flags=IMG_INIT_JPG|IMG_INIT_PNG;
    int initted= 0;
    initted = IMG_Init(flags);

    if((initted&flags) != flags) 
    {
        printf("IMG_Init: Impossible d'initialiser le support des formats JPG et PNG requis!\n");
        printf("IMG_Init: %s\n", IMG_GetError());
    }
    
    //------------------------------------------------------------------
    /* Création du renderer */
    SDL_Renderer* renderer = NULL;
    renderer = SDL_CreateRenderer(
            window, -1, 0);
    if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", NULL, window, renderer);

    std::chrono::time_point<std::chrono::system_clock>start,end;
    std::chrono::duration<double> elapsed_seconds;
    const int fps = 10;
    const float inv_fps_ms = 1000.f/fps;
    // Uint64 start_f;
    Uint64 end_f;
    float elapsedMS_f;
    Time time(2*fps);
    
    


    std::string input = "";
    if (argc == 2){//il y a 1 param
        input = argv[1];
    }
    Controller controller(input);
    Dessin dessin;
    SDL_Rect box;
    setup_box(box,largeur,hauteur);
    // controller.print_words();


    bool program_on = true;
    std::cout << "Commencement du program" << std::endl;
    while (program_on) {                              // La boucle des évènements
        time.addTime(SDL_GetPerformanceCounter());
        if (controller.getTyping(0).getWord() == "") start = std::chrono::system_clock::now();
        // Uint32 startTicks = SDL_GetTicks();
		// Uint64 startPerf = SDL_GetPerformanceCounter();
        // ++totalFrames;
        SDL_Event event;                                // Evènement à traiter

        while (program_on && SDL_PollEvent(&event)) {   // Tant que la file des évènements stockés n'est pas vide et qu'on n'a pas
                                                        // terminé le programme Défiler l'élément en tête de file dans 'event'
            if ((event.type==SDL_KEYDOWN) && (event.key.keysym.sym==SDLK_LSHIFT)) controller.pressLshift();
            if ((event.type==SDL_KEYDOWN) && (event.key.keysym.sym==SDLK_RSHIFT)) controller.pressRshift();
            if ((event.type==SDL_KEYUP) && (event.key.keysym.sym==SDLK_LSHIFT)) controller.unpressLshift();
            if ((event.type==SDL_KEYUP) && (event.key.keysym.sym==SDLK_RSHIFT)) controller.unpressRshift();
            if ((event.type==SDL_KEYDOWN) && (event.key.keysym.sym==SDLK_LALT)) controller.pressLalt();
            if ((event.type==SDL_KEYDOWN) && (event.key.keysym.sym==SDLK_RALT)) controller.pressRalt();
            if ((event.type==SDL_KEYUP) && (event.key.keysym.sym==SDLK_LALT)) controller.unpressLalt();
            if ((event.type==SDL_KEYUP) && (event.key.keysym.sym==SDLK_RALT)) controller.unpressRalt();
            
            switch (event.type) {                         // En fonction de la valeur du type de cet évènement
                case SDL_QUIT:                                // Un évènement simple, on a cliqué sur la x de la // fenêtre
                    program_on = false;                     // Il est temps d'arrêter le programme
                    break;
                case SDL_KEYDOWN:
                    std::cout << (int) event.key.keysym.sym << std::endl;
                    
                    switch (event.key.keysym.sym){
                    case SDLK_BACKSPACE://la touche pour supprimer
                        controller.pressDel();
                        break;
                    case SDLK_SPACE:
                        controller.pressSpace();
                        break;
                    default:
                        controller.pressLetter(event.key.keysym.sym);
                        break;
                    }
                    // if (isalpha_cor(event.key.keysym.sym)){
                    //     std::cout << "ce qui donne : " << (char) event.key.keysym.sym << std::endl;
                    // }
                    break;
                case SDL_WINDOWEVENT:
                    if(event.window.event == SDL_WINDOWEVENT_RESIZED) {
                        SDL_GetWindowSize(window, &largeur,&hauteur);
                        std::cout << "largeur : " << largeur << " | hauteur " << hauteur << std::endl;
                        setup_box(box,largeur,hauteur);
                        dessin.MAJ(box,controller,font);
                        dessin.print_nb();






                    }
                    break;
            }
        }
        end = std::chrono::system_clock::now();
        elapsed_seconds = end - start;
        

        // Do event loop

        // Do physics loop

        // Do rendering loop
        afficher_fond(renderer,window);
        afficher_temps(elapsed_seconds.count(),font,renderer,window);
        afficher_nb_mots_tot(controller.getNbMotsTot(), font , renderer,  window);
        afficher_nb_char_ecrit(controller,font,renderer,window);
        afficher_nb_mot_ecrit(controller,font,renderer,window);
        afficher_precision(controller.getPrecision(),font,renderer,window);
        dessin.afficher_words_typing(box,controller,font,renderer,window);//marche bien
        // afficher_typing(controller,font,renderer,window);
        // time.display_all_time();

        // End frame timing
        end_f = SDL_GetPerformanceCounter();
        elapsedMS_f = (end_f - time.getlast()) / (float)SDL_GetPerformanceFrequency();
        // std::cout << "elapsed ms:" << elapsedMS_f<< " obj ms "<< inv_fps_ms << "fsp " << time.getfps() << std::endl;
        // Cap to FPS
        if (inv_fps_ms > elapsedMS_f) SDL_Delay(floor(inv_fps_ms - elapsedMS_f));
        // afficher_fps(((float)SDL_GetPerformanceFrequency())/time.getfps(),font,renderer,window);
        afficherEcran(renderer);
    }
    end_sdl(0, "Normal ending", font, window, renderer);//renderer
    return EXIT_SUCCESS;
}