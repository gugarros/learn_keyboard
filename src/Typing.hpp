#ifndef TYPING_HPP
#define TYPING_HPP

#include <string>
#include "Word.hpp"


//a enlever
#include <iostream>


class Typing:public Word{
private:
public:
    Typing();
    Typing(std::string mot);
    void addLetter(std::string letter);
    std::string getLast()const;
    void supprLetter();
};

int is_same(const Word &w, const Typing &t);


#endif