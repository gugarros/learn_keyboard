#ifndef DESSIN_CPP
#define DESSIN_CPP
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include <string>

#include "Controller.hpp"

//il ne faut pas mettre le "class" car sinon on ne peut pas faire & ou |
enum POS {  LEFT=1,
            RIGHT=2,
            TOP=4,
            BOT=8,
            MID=16
};


class Dessin{
    private:
    int first_i_at_first_line;
    std::vector<int> nb_mot_par_ligne;

    public:
    Dessin();
    void MAJ(SDL_Rect box,Controller &controller,TTF_Font * font);
    void afficher_words_typing(SDL_Rect box,Controller &controller,TTF_Font * font, SDL_Renderer* renderer,SDL_Window *window);

    void print_nb() const;
};


void setup_box(SDL_Rect & box,int largeur, int hauteur);
void end_sdl(char ok, char const* msg, TTF_Font * font, SDL_Window* window, SDL_Renderer* renderer);
void afficher_fond(SDL_Renderer* renderer,SDL_Window *window);
SDL_Rect afficher_text(POS p,std::string text,int x,int y,SDL_Color color,TTF_Font * font,SDL_Renderer* renderer,SDL_Window *window);
void afficher_temps(float temps, TTF_Font * font,SDL_Renderer* renderer,SDL_Window *window);
void afficher_nb_mots_tot(int nb, TTF_Font * font,SDL_Renderer* renderer,SDL_Window *window);
void afficher_fps(float f1, TTF_Font * font,SDL_Renderer* renderer,SDL_Window *window);
void afficher_precision(float prec, TTF_Font * font,SDL_Renderer* renderer,SDL_Window *window);
void afficher_nb_mot_ecrit(const Controller &controller, TTF_Font * font,SDL_Renderer* renderer,SDL_Window *window);
void afficher_nb_char_ecrit(const Controller &controller, TTF_Font * font,SDL_Renderer* renderer,SDL_Window *window);
void afficher_typing(const Controller &controller, TTF_Font * font,SDL_Renderer* renderer,SDL_Window *window);
//voué a disparaitre
// void afficher_words(const Controller &controller, TTF_Font * font,SDL_Renderer* renderer,SDL_Window *window);
//voué a disparaitre
void afficherEcran(SDL_Renderer *renderer);

#endif