#include "Controller.hpp"

Controller::Controller(std::string nom_fichier,std::string lettres):
    nb_mots_juste(0),
    nb_mots_faux(0),
    nb_char_juste(0),
    nb_char_faux(0),
    lshift(false),
    rshift(false),
    lalt(false),
    ralt(false),
    chapeau(false),
    deuxpoints(false),
    temps(0.f)
{

    typings.push_back(Typing());


    std::ifstream flux(nom_fichier);
    std::string line;
    std::string::size_type n;
    bool est_dedans;
    unsigned long int i;
    if(flux) {
        std::cout<<"lecture du fichier "<< nom_fichier <<std::endl;
        while(getline(flux,line))
            // std::cout<<line<<std::endl;
            if (lettres == ""){
                words.push_back(line);
            } else {
                if (lettres == "^"){
                    lettres = ".âêîôû";
                }
                if (lettres == "¨"){
                    lettres = ".ëïöü";
                }

                if (lettres[0] == '.'){//union
                    i = 1;
                    while (i<lettres.length()){
                        if(lettres[i] == -61) ++i;//marche car il n'y a pas -61 en 2eme
                        n = line.find(lettres[i]);
                        if (n!=std::string::npos){
                            words.push_back(line);
                            i = lettres.length();//pour sortir de la boucle
                        }
                        ++i;
                    }
                } else {//intersection marche quand meme pour les caractères spéciaux car intersection
                    est_dedans = true;
                    i = 0;
                    while (est_dedans && i<lettres.length()){
                        n = line.find(lettres[i]);
                        if (n==std::string::npos){
                            est_dedans = false;
                        }
                        ++i;
                    }
                    if (est_dedans) words.push_back(line);
                }
            }
    } else {
        std::cout<<"impossible d'ouvrir le fichier" <<std::endl;
    }
    std::random_device rd;
    std::default_random_engine rng(rd());
    std::shuffle(words.begin(), words.end(), rng);
    flux.close();
};

Controller::Controller(std::string lettres):Controller("../res/francais_dico.txt",lettres){

}
Controller::Controller():Controller("../res/francais_dico.txt",""){
    
}

void Controller::print_words() const {
    std::cout << "Affichage vector" << std::endl;
    for(unsigned long int i=0; i < words.size(); i++)
    std::cout << words[i].getWord() << std::endl;//il manque peut etre le dernier element
    std::cout << "Fin affichage vector" << std::endl;
}


void Controller::pressLetter(int letter){
    //'a' -> 'z'
    //'0' -> '9'
    //, ; : ! ù * $ ^ = )
    std::string add;

    // std::cout << "derniere lettre : " << typings.back().getLast() << std::endl;
    if ( typings.back().getLast() == "^"){
        switch (letter){
        case 'a':
            add = "â";
            break;
        case 'e':
            add = "ê";
            break;
        case 'i':
            add = "î";
            break;
        case 'o':
            add = "ô";
            break;
        case 'u':
            add = "û";
            break;
        default:
            break;
        }
    }
    if ( typings.back().getLast() == "¨"){
        switch (letter){
        case 'a':
            add = "ä";
            break;
        case 'e':
            add = "ë";
            break;
        case 'i':
            add = "ï";
            break;
        case 'o':
            add = "ö";
            break;
        case 'u':
            add = "ü";
            break;
        default:
            break;
        }
    } 
    if (add != ""){//l'ajout de la lettre
        pressDel();//il faut pas rajouter une erreur
        letter = '\0';//pour pas aller dans la suite
    }
    
    if (isShift()){
        if (isdigit_cor(letter)){
            add = letter;
        }
        if (isalpha_cor(letter)){
            add = letter - 'a' + 'A';//met la lettre en majuscule
        }
        switch (letter){
        case ',':
            add = "?";
            break;
        case ';':
            add = ".";
            break;
        case ':':
            add = "/";
            break;
        case '!':
            add = "+";
            break;
        case '^':
            add = "¨";
            break;
        default:
            break;
        }
    } else {//no shift
        if(isalpha_cor(letter)){
            add = letter;
        } else {
            switch (letter){
            case ',':
                add = ",";
                break;
            case ';':
                add = ";";
                break;
            case ':':
                add = ":";
                break;
            case '!':
                add = "!";
                break;
            case '^':
                add = "^";
                break;
            case '0':
                add = "à";
                break;
            case '1':
                add = "&";
                break;
            case '2':
                add = "é";
                break;
            case '3':
                add = "\"";
                break;
            case '4':
                add = "'";
                break;
            case '5':
                add = "(";
                break;
            case '6':
                add = "-";
                break;
            case '7':
                add = "è";
                break;
            case '8':
                add = "_";
                break;
            case '9':
                add = "ç";
                break;
            default:
                break;
            }
        }
    } 



    if (add != "") {
        std::cout << "New letter : " << add << std::endl;
        typings.back().addLetter(add);
        std::cout << "size: " <<typings.size()<< std::endl;


        if ((add != "^") &&(add != "¨")){
            if (0 == is_same(words[typings.size()-1],typings.back())){//égaux
                nb_char_juste++;
            } else {
                nb_char_faux++;
            }
        }
    }
}

void Controller::pressSpace(){
    if (0 == typings.back().getWord().compare(words[typings.size()-1].getWord())){//égaux
        nb_mots_juste++;
    } else {
        nb_mots_faux++;
    }
    typings.push_back(Typing());
}
void Controller::pressDel(){
    if (typings.back().getWord() != ""){
        if (0 == is_same(words[typings.size()-1],typings.back())){//égaux
                nb_char_juste--;
            } else {
                //nb_char_faux--;(faux car ca reste une erreur)
            }
        typings.back().supprLetter();
        //normalement ça marche bien
        //il faut enlever les caractères faux (faux car ca reste une erreur)
    } else {
        typings.pop_back();
        if (0 == typings.back().getWord().compare(words[typings.size()-1].getWord())){//égaux
            nb_mots_juste--;
        } else {
            nb_mots_faux--;
        }
    }
}

bool isalpha_cor(int c){//si on met char ca bug car char plus petit que int
    return (bool) (((c>='a') && (c<='z')));
}
bool isdigit_cor(int c){
    return (bool) (((c>='0') && (c<='9')));
}