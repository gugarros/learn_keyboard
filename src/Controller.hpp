#ifndef CONTROLLER_HPP
#define CONTROLLER_HPP

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <random>
#include <chrono>

#include "Word.hpp"
#include "Typing.hpp"

class Controller{
private:
    int nb_mots_juste;
    int nb_mots_faux;
    int nb_char_juste;
    int nb_char_faux;

    bool lshift;
    bool rshift;
    bool lalt;
    bool ralt;
    bool chapeau;
    bool deuxpoints;
    float temps;

    std::vector<Word> words;
    std::vector<Typing> typings;
public:
    void pressLshift(){lshift = true;};
    void pressRshift(){rshift = true;};
    void pressLalt(){lalt = true;};
    void pressRalt(){ralt = true;};
    void unpressLshift(){lshift = false;};
    void unpressRshift(){rshift = false;};
    void unpressLalt(){lalt = false;};
    void unpressRalt(){ralt = false;};

    void pressLetter(int);
    void pressSpace();
    void pressDel();

    Controller(std::string nom_fichier,std::string lettres);
    Controller(std::string lettres);
    Controller();
    void print_words() const;
    int getNbMotsTot() const{return words.size();};
    int getNbMotsJuste() const{return nb_mots_juste;};
    int getNbMotsFaux() const{return nb_mots_faux;};
    int getNbCharJuste() const{return nb_char_juste;};
    int getNbCharFaux() const{return nb_char_faux;};
    const Typing& getTyping(int i) const{return typings[i];};
    const Word& getWord(int i) const{return words[i];};
    int getSizeTyping() const{return typings.size();};
    float getPrecision() const{
        float f;
        if (nb_char_juste == 0){
            f = 1.;
        } else {
            f = (nb_char_juste-nb_char_faux);
            f /= nb_char_juste;
            f *= nb_mots_juste-nb_mots_faux;
            f /= nb_mots_juste;
        }
        return f*100;
    };
    bool isAlt() const{return (lalt || ralt);};
    bool isShift() const{return (lshift || rshift);};
    void print_all_typing()const {
        std::cout<< "affichage tous le typing" << std::endl;
        for(long unsigned int i = 0; i<typings.size();++i) {
            std::cout<< typings[i].getWord() << std::endl;
        }
    }
};

bool isalpha_cor(int c);
bool isdigit_cor(int c);

#endif