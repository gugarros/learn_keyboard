#include "Typing.hpp"


Typing::Typing(std::string mot):Word(mot){
}

Typing::Typing():Word(){
}

void Typing::addLetter(std::string letter){
    for (unsigned long int i=0;i<letter.size();++i){
        getWord().push_back(letter[i]);
    }
    getNbLettres()++;
}

void Typing::supprLetter(){
    if(getWord().back() < 0){
        getWord().pop_back();
    }
    getWord().pop_back();
    getNbLettres()--;
}

std::string Typing::getLast()const{
    std::string last = "";
    int n = getWord().size();
    // std::cout << "carac fin :" << getWord().c_str()[n-1] << std::endl;
    if (getWord().c_str()[n-1] <= 0){//carac spécial
        last = getWord().c_str()[n-2];
        last += getWord().c_str()[n-1];;
        // last.append(getWord().c_str()[n-2]);
    } else {//simple caractere
        last = getWord().back();
    }
    return last;
}



int is_same(const Word & w, const Typing & t){
    //renvoi 0 si pareil
    //renvoi i + 1 si différent
    //compare jusqu'a taille de typing
    int res = 0;
    int n = t.getWord().size();
    int i=0;
    std::string last = t.getLast();
    if ((last == "^") || (last == "¨")){
        n -= last.size();//enleve 1 ou 2
    }
    while (i<n && res == 0){
        if (w.getWord().c_str()[i] != t.getWord().c_str()[i]) {
            res = i+1;
        }
        ++i;
    }
    // std::cout << "mot1 :"<<w.getWord()<<" mot2:" << t.getWord()<< " i : "<< i << std::endl;
    return res;
}