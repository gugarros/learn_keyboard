#include "Time.hpp"

Time::Time(int nn):n(nn),rn(0),i(-1),t(new Uint64 [n]){

}

Time::~Time(){
    delete t;
}

void Time::addTime(const Uint64 & ii ){
    i = (i+1) % n;
    t[i]=ii;
    if (rn<n) rn++;
}

float Time::getfps() const{
    Uint64 res = 0;
    Uint64 last = t[i];
    int ii;
    for(int j=0;j<rn;++j){
        ii = (i-j)%n;
        res += last - t[ii];
        last = t[ii];
    }
    // std::cout << "res " << res << "real " << res/ (float) (rn-1)/ (float)  SDL_GetPerformanceFrequency()<<std::endl;
    return res/ (float) (rn-1);
}

Uint64 Time::getlast() const {
    return t[i];
};

void Time::display_all_time() const {
    for(int ii = 0; ii<rn;++ii){
        std::cout << " temps" << ii << " " << t[ii] <<std::endl;
    }
}