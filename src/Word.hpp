#ifndef WORD_HPP
#define WORD_HPP

#include <string>

class Word{
private:
    std::string mot;
    int nb_lettres;
public:
    Word();
    Word(std::string mot);
    const std::string &getWord() const {return mot;};
    std::string & getWord() {return mot;};
    int getNbLettres() const {return nb_lettres;};
    int & getNbLettres() {return nb_lettres;};


};




#endif