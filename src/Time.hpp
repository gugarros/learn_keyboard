#ifndef TIME_HPP
#define TIME_HPP


#include <iostream>
#include <SDL2/SDL.h>
//pour le Uint64

class Time{
private:
    int n;
    int rn;
    int i;
    Uint64 * t;
public:
    Time(int);
    ~Time();
    void addTime(const Uint64 &);
    float getfps() const;
    Uint64 getlast() const;
    void display_all_time() const;

}
;


#endif