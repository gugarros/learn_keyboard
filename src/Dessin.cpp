#include "Dessin.hpp"


Dessin::Dessin():first_i_at_first_line(0){
    //ligne vas va être modifier
}

void Dessin::MAJ(SDL_Rect box,Controller &controller,TTF_Font * font){
    SDL_Color color = {255,255,255,255};
    SDL_Surface* text_surface = NULL;
    char text[128];
    int decalage = 0;
    int indice = first_i_at_first_line;
    int ligne = 0;
    int ligne_decalage = 0;
    // int t_space = 10;
    int nb_mots = 0;

    nb_mot_par_ligne.clear();
    // std::cout<< "box "<< box.x << ":" <<box.y << ":" <<box.w << ":" <<box.h << std::endl;
    while (indice < controller.getNbMotsTot() && ligne_decalage <= box.h){
        sprintf(text,"%s ",controller.getWord(indice).getWord().c_str());
        if (text[0] != '\0'){
            text_surface = TTF_RenderUTF8_Blended(font, text, color);
            if (decalage+text_surface->w >= box.w){//si ca depasse
                nb_mot_par_ligne.push_back(nb_mots);
                // std::cout<<"nb_mots"<< nb_mots<<std::endl;
                ligne++;
                ligne_decalage += text_surface->h;
                nb_mots = 0;
                decalage = 0;
            }
            decalage += text_surface -> w;
        }
        ++nb_mots;
        // decalage += t_space;
        ++indice;
    }
    SDL_FreeSurface(text_surface); // la texture ne sert plus à rien
}
void Dessin::afficher_words_typing(SDL_Rect box,Controller &controller,TTF_Font * font, SDL_Renderer* renderer,SDL_Window *window){
    SDL_Color gray = {128,128,128,255};
    SDL_Color white = {255,255,255,255};
    SDL_Color red = {255,64,64,255};
    SDL_Color *color;
    SDL_Rect pos = {0, 0, 0, 0};
    SDL_Surface* text_surface = NULL;
    SDL_Texture* text_texture = NULL;
    char text[128];
    int ligne = 0;
    // int t_space = 10;
    int decalage;
    int indice;
    int index;
    int nb_word_print = 0;
    int add_decalage;

    // first_i_at_first_line = 7;
    int apres_le_print_faux_changer_lattribut=0;

    //affiche que les words pour l'instant
    for(ligne = 0 ; ligne < (int) nb_mot_par_ligne.size() ; ++ligne){
        decalage = 0;
        for(index = 0; index < nb_mot_par_ligne[ligne];++index){
            add_decalage = 0;
            indice = index + first_i_at_first_line+nb_word_print;

            //il faut tester si les mots sont bon ou pas
            //et écrire en blanc / gris / rouge selon is_same()
            if(indice < controller.getSizeTyping()){
                if (0 == is_same(controller.getWord(indice).getWord(),controller.getTyping(indice).getWord())){//égaux
                    color = &gray;
                    sprintf(text,"%s ",controller.getWord(indice).getWord().c_str());
                    if (text[0] != '\0'){
                        text_surface = TTF_RenderUTF8_Blended(font, text, *color);
                        if (text_surface == NULL) end_sdl(0, "Can't create text surface", font, window, renderer);
                        text_texture = SDL_CreateTextureFromSurface(renderer, text_surface);
                        if (text_texture == NULL) end_sdl(0, "Can't create texture from surface", font, window, renderer);
                        SDL_FreeSurface(text_surface);
                        SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h);
                        pos.y = box.y+ligne*pos.h;
                        pos.x = box.x+decalage;
                        SDL_RenderCopy(renderer, text_texture, NULL, &pos);
                        SDL_DestroyTexture(text_texture);
                        // decalage += pos.w;
                        add_decalage = pos.w;
                    }
                    color = &white;
                    sprintf(text,"%s ",controller.getTyping(indice).getWord().c_str());
                    if (ligne == 2){
                        apres_le_print_faux_changer_lattribut = nb_mot_par_ligne[0];
                        // first_i_at_first_line = indice+1;
                        std::cout << "Rajout de iiiii (première ligne) : " << apres_le_print_faux_changer_lattribut << std::endl;
                    }
                } else {
                    color = &red;
                    sprintf(text,"%s ",controller.getTyping(indice).getWord().c_str());
                }
            } else {
                color = &gray;
                sprintf(text,"%s ",controller.getWord(indice).getWord().c_str());
            }
            // std::cout << "cheval" <<text[0] << std::endl;
            if (text[0] != '\0'){
                // if (add_decalage) decalage -= pos.w;
                text_surface = TTF_RenderUTF8_Blended(font, text, *color);
                if (text_surface == NULL) end_sdl(0, "Can't create text surface", font, window, renderer);
                text_texture = SDL_CreateTextureFromSurface(renderer, text_surface);
                if (text_texture == NULL) end_sdl(0, "Can't create texture from surface", font, window, renderer);
                SDL_FreeSurface(text_surface);
                SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h);
                pos.y = box.y+ligne*pos.h;
                pos.x = box.x+decalage;
                SDL_RenderCopy(renderer, text_texture, NULL, &pos);
                SDL_DestroyTexture(text_texture);
                if (add_decalage==0){
                    decalage += pos.w;    
                } else {
                    decalage += add_decalage;    
                }
            }
            // decalage += t_space;
        }   
        nb_word_print += nb_mot_par_ligne[ligne];
    }

    if (apres_le_print_faux_changer_lattribut) {
        first_i_at_first_line += apres_le_print_faux_changer_lattribut;
        this->MAJ(box,controller,font);
    }
}



void Dessin::print_nb() const{
    for (int i=0; i < (int) nb_mot_par_ligne.size();++i){
        std::cout<< "ligne : " << i << " | nb : " << nb_mot_par_ligne[i] << std::endl;
    }
}


void setup_box(SDL_Rect & box,int largeur, int hauteur){
    box.x = 100;
    box.y = 100;
    box.w = largeur - 200;
    box.h = hauteur - 400;
}







void end_sdl(char ok,                                                 // fin normale : ok = 0 ; anormale ok = 1
                    char const* msg,                                    // message à afficher
                    TTF_Font * font,
                    SDL_Window* window,                                 // fenêtre à fermer
                    SDL_Renderer* renderer) {                           // renderer à fermer
    char msg_formated[255];                                         
    int l;                                                          

    if (!ok) {                                                      
            strncpy(msg_formated, msg, 250);                                 
            l = strlen(msg_formated);                                        
            strcpy(msg_formated + l, " : %s\n");                     

            SDL_Log(msg_formated, SDL_GetError());                   
    }                                                               

    if (renderer != NULL) SDL_DestroyRenderer(renderer);                            
    if (window != NULL)   SDL_DestroyWindow(window);
    if (font != NULL) TTF_CloseFont(font);
    IMG_Quit();
    TTF_Quit();
    SDL_Quit();                                                     

    if (!ok) {                                                      
        exit(EXIT_FAILURE);                                              
    }                                                               
}


void afficher_fond(SDL_Renderer* renderer,SDL_Window *window){//SDL_Texture* fond
    SDL_Rect window_dimensions;
    SDL_GetWindowSize(
      window, &window_dimensions.w,
      &window_dimensions.h); 
    
    SDL_SetRenderDrawColor(renderer,0,0,0,255);
    SDL_Rect rectangle;
    rectangle.x=0;
    rectangle.y=0;
    rectangle.w=window_dimensions.w;
    rectangle.h=window_dimensions.h;
    SDL_RenderFillRect(renderer,&rectangle);
}


SDL_Rect afficher_text(POS p,char * text,int x,int y,SDL_Color color,TTF_Font * font,SDL_Renderer* renderer,SDL_Window *window){
    SDL_Surface* text_surface = NULL;
    SDL_Rect window_dimensions;
    SDL_GetWindowSize(window, &window_dimensions.w,&window_dimensions.h);

    text_surface = TTF_RenderUTF8_Blended(font, text, color);
    if (text_surface == NULL) end_sdl(0, "Can't create text surface", font, window, renderer);

    SDL_Texture* text_texture = NULL;                                    // la texture qui contient le texte

    text_texture = SDL_CreateTextureFromSurface(renderer, text_surface); // transfert de la surface à la texture
    if (text_texture == NULL) end_sdl(0, "Can't create texture from surface", font, window, renderer);
    SDL_FreeSurface(text_surface);                                       // la texture ne sert plus à rien

    SDL_Rect pos = {0, 0, 0, 0};                                         // rectangle où le texte va être prositionné
    SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h);          // récupération de la taille (w, h) du texte 
    
    if (p & TOP) {
        pos.y = y;
    }
    if (p & LEFT) {
        pos.x = x;
    }
    if (p & BOT) {
        pos.y = y - pos.h + window_dimensions.h;
    }
    if (p & RIGHT){
        pos.x = x- pos.w + window_dimensions.w;
    }
    //MID va ecraser les valeurs (enfin elle sont vide en vrai)
    if ((p & MID) && ((p & TOP) || (p & BOT))){
        pos.x = x -pos.w/2 +window_dimensions.w/2;
    }
    if ((p & MID) && ((p & LEFT) || (p & RIGHT))){
        pos.y = y -pos.h/2 +window_dimensions.h/2;
    }


    SDL_RenderCopy(renderer, text_texture, NULL, &pos);    // Ecriture du texte dans le renderer   
    SDL_DestroyTexture(text_texture);                                    // On n'a plus besoin de la texture avec le texte
    return pos;
}

void afficher_temps(float temps, TTF_Font * font,SDL_Renderer* renderer,SDL_Window *window){
    SDL_Color color = {0,255,255,255};
    POS p = (POS) (BOT | RIGHT);
    char text[128];
    sprintf(text,"Temps : %.1f",temps);
    afficher_text(p,text,-10,-10,color,font,renderer,window);
}

void afficher_nb_mots_tot(int nb, TTF_Font * font,SDL_Renderer* renderer,SDL_Window *window){
    SDL_Color color = {0,128,255,255};
    POS p = (POS) (TOP | RIGHT);
    char text[128];
    sprintf(text,"Nb tot : %d",nb);
    afficher_text(p,text,-10,10,color,font,renderer,window);
}

void afficher_fps(float f1, TTF_Font * font,SDL_Renderer* renderer,SDL_Window *window){
    SDL_Color color = {255,255,255,255};
    POS p = (POS) (TOP | RIGHT);
    char text[128];
    sprintf(text,"Current FPS: %3.3f",f1);
    afficher_text(p,text,-10,10,color,font,renderer,window);
}

void afficher_precision(float prec, TTF_Font * font,SDL_Renderer* renderer,SDL_Window *window){
    SDL_Color color = {158,133,184,255};
    POS p = (POS) (BOT | MID);
    char text[128];
    sprintf(text,"Précision %.2f%%",prec);
    afficher_text(p,text,-10,-10,color,font,renderer,window);
}

void afficher_nb_mot_ecrit(const Controller &controller, TTF_Font * font,SDL_Renderer* renderer,SDL_Window *window){
    SDL_Color green = {0,255,0,255};
    SDL_Color red = {255,0,0,255};
    SDL_Color white = {255,255,255,255};
    POS p = (POS) (BOT | LEFT);
    SDL_Rect pos = {0, 0, 0, 0};
    int decalage = 10;
    char text[128];
    sprintf(text,"Mots %d (",controller.getNbMotsJuste()+controller.getNbMotsFaux());
    pos = afficher_text(p,text,decalage,-10,white,font,renderer,window);
    decalage += pos.w;
    sprintf(text,"%d",controller.getNbMotsJuste());
    pos = afficher_text(p,text,decalage,-10,green,font,renderer,window);
    decalage += pos.w;
    sprintf(text,",");
    pos = afficher_text(p,text,decalage,-10,white,font,renderer,window);
    decalage += pos.w;
    sprintf(text,"%d",controller.getNbMotsFaux());
    pos = afficher_text(p,text,decalage,-10,red,font,renderer,window);
    decalage += pos.w;
    sprintf(text,")");
    afficher_text(p,text,decalage,-10,white,font,renderer,window);
}

void afficher_nb_char_ecrit(const Controller &controller, TTF_Font * font,SDL_Renderer* renderer,SDL_Window *window){
    SDL_Color green = {0,255,0,255};
    SDL_Color red = {255,0,0,255};
    SDL_Color gray = {180,180,180,255};
    POS p = (POS) (BOT | LEFT);
    SDL_Rect pos = {0, 0, 0, 0};
    int decalage = 10;
    char text[128];
    sprintf(text,"Char %d (",controller.getNbCharJuste()+controller.getNbCharFaux());
    pos = afficher_text(p,text,decalage,-60,gray,font,renderer,window);
    decalage += pos.w;
    sprintf(text,"%d",controller.getNbCharJuste());
    pos = afficher_text(p,text,decalage,-60,green,font,renderer,window);
    decalage += pos.w;
    sprintf(text,",");
    pos = afficher_text(p,text,decalage,-60,gray,font,renderer,window);
    decalage += pos.w;
    sprintf(text,"%d",controller.getNbCharFaux());
    pos = afficher_text(p,text,decalage,-60,red,font,renderer,window);
    decalage += pos.w;
    sprintf(text,")");
    afficher_text(p,text,decalage,-60,gray,font,renderer,window);
}

void afficher_typing(const Controller &controller, TTF_Font * font,SDL_Renderer* renderer,SDL_Window *window){
    SDL_Color yellow = {255,255,0,255};
    SDL_Color white = {255,255,255,255};
    POS p = (POS) (TOP | LEFT);
    SDL_Rect box = {100, 100, 0, 0};
    SDL_GetWindowSize(window, &box.w,&box.h);
    box.w-=200;box.h-=200;
    SDL_Rect pos = {0, 0, 0, 0};
    pos.x = box.x;
    pos.y = box.y;
    char text[128];
    int decalage = 0;
    int indice = 0;
    int ligne = 0;
    static int frame = 0;
    int t_space = 10;

    while (indice < controller.getSizeTyping()){
        sprintf(text,"%s",controller.getTyping(indice).getWord().c_str());
        if (text[0] != '\0'){
            pos = afficher_text(p,text,box.x+decalage,box.y+ligne*pos.h,white,font,renderer,window);
            if (decalage+pos.w >= box.w){//si ca depasse
                ligne++;
                decalage = 0;
            }
            pos = afficher_text(p,text,box.x+decalage,box.y+ligne*pos.h,white,font,renderer,window);
            decalage += pos.w;
        }
        decalage += t_space;
        ++indice;
    }
    frame ++;
    if (frame >=30) frame = 0;
    sprintf(text,"%s","|");
    if (frame % 10 <= 5) afficher_text(p,text,box.x+decalage-t_space,box.y+ligne*pos.h,yellow,font,renderer,window);
    // controller.print_all_typing();
}

// void afficher_words(const Controller &controller, TTF_Font * font,SDL_Renderer* renderer,SDL_Window *window){
//     SDL_Color red = {255,0,0,255};
//     SDL_Color yellow = {255,255,0,255};
//     SDL_Color white = {255,255,255,255};


//     // il faut tout refaire =>
//     // faire afficher word qui crée une structure qui indique le nb de mots par ligne
//     // et la position de la ligne courante
//     // puis afficher typing par dessus

//     POS p = (POS) (TOP | LEFT);
//     SDL_Rect box = {100, 100, 0, 0};
//     SDL_GetWindowSize(window, &box.w,&box.h);
//     box.w-=200;box.h-=200;
//     SDL_Rect pos = {0, 0, 0, 0};
//     pos.x = box.x;
//     pos.y = box.y;
//     char text[128];
//     int decalage = 0;
//     int indice = 0;
//     int ligne = 0;
//     static int frame = 0;
//     int t_space = 10;

//     while (indice < controller.getNbMotsTot() && indice <= 100){
//         sprintf(text,"%s",controller.getWord(indice).getWord().c_str());
//         if (text[0] != '\0'){
//             pos = afficher_text(p,text,box.x+decalage,box.y+ligne*pos.h,white,font,renderer,window);
//             if (decalage+pos.w >= box.w){//si ca depasse
//                 ligne++;
//                 decalage = 0;
//             }
//             pos = afficher_text(p,text,box.x+decalage,box.y+ligne*pos.h,white,font,renderer,window);
//             decalage += pos.w;
//         }
//         decalage += t_space;
//         ++indice;
//     }
//     frame ++;
//     if (frame >=30) frame = 0;
//     sprintf(text,"%s","|");
//     if (frame % 10 <= 5) afficher_text(p,text,box.x+decalage-t_space,box.y+ligne*pos.h,yellow,font,renderer,window);
//     // controller.print_all_typing();
// }

void afficherEcran(SDL_Renderer *renderer){
    SDL_RenderPresent(renderer);
}