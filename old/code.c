#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
// #include "lch.c"
//-- 29 mots par minute --- 31 corrections --- Presicion 69%
//https://www.lecturel.com/clavier/mots-par-minute.php
//but 126 - 148
//https://monkeytype.com/
//--- 40 mots par minute --- 20 corrections --- Presicion 80%

#define LECON 0
#define EXERCICE 0
#define NOMBRE 0
#define NOMBRE_ERREUR 4
#define TAILLE_BUFFER 2048



// Pour récupérer une ligne dans un fichier texte, tu peux utiliser "fgetline".
// "srand" te permet d'initialiser le générateur de nombre aléatoire
// (à n'appeler qu'une seul fois au début de ton main), et "rand"
// te permet d'obtenir un nombre aléatoire.



//temps : https://www.delftstack.com/fr/howto/c/gettimeofday-in-c/
float time_diff(struct timeval *start, struct timeval *end)
{
    return (end->tv_sec - start->tv_sec) + 1e-6*(end->tv_usec - start->tv_usec);
}

void test_erreur(int * erreur, char * resultat, char * text){
    int n = strlen(text);
    //int i =0;
    //while (text[i] != '\0' && resultat[i] != '\0' && text[i] == resultat[i]) ++i;
    if (n >1){
        if (!strncmp(text,resultat,n-1) && strncmp(text,resultat,n)){
            *erreur = *erreur +1;
        }
    }
}

int strcomptocc(char * text,char lettre){
    int n = 0;
    int i = 0;
    while (text[i] != '\0'){
        if (text[i++] == lettre) ++n;
    }
    return n;
}

void reset(char * buff){
    for(int i=0;i<TAILLE_BUFFER;++i){
        buff[i] = '\0';
    }
    //buff[0] = ' ';
}

void assigne(char * resultat, char ** text[18],int exercice, int lecon,int nombre_text[][8]){
    resultat[0] = '\0';
    int nombre = nombre_text[lecon][exercice];
    for(int i =0;i<nombre;++i){
        strcat(resultat,text[lecon][exercice]);
        if (i != nombre) strcat(resultat," ");
    }
}

void afficherEcran(SDL_Renderer *renderer){
    SDL_RenderPresent(renderer);
}

void end_sdl(char ok,                                                 // fin normale : ok = 0 ; anormale ok = 1
                    char const* msg,                                    // message à afficher
                    TTF_Font * font,
                    SDL_Window* window,                                 // fenêtre à fermer
                    SDL_Renderer* renderer) {                           // renderer à fermer
    char msg_formated[255];                                         
    int l;                                                          

    if (!ok) {                                                      
            strncpy(msg_formated, msg, 250);                                 
            l = strlen(msg_formated);                                        
            strcpy(msg_formated + l, " : %s\n");                     

            SDL_Log(msg_formated, SDL_GetError());                   
    }                                                               

    if (renderer != NULL) SDL_DestroyRenderer(renderer);                            
    if (window != NULL)   SDL_DestroyWindow(window);
    if (font != NULL) TTF_CloseFont(font);
    IMG_Quit();
    TTF_Quit();
    SDL_Quit();                                                     

    if (!ok) {                                                      
        exit(EXIT_FAILURE);                                              
    }                                                               
}

void afficher_fond(SDL_Renderer* renderer,SDL_Window *window){//SDL_Texture* fond
    SDL_Rect window_dimensions;
    SDL_GetWindowSize(
      window, &window_dimensions.w,
      &window_dimensions.h); 
    
    SDL_SetRenderDrawColor(renderer,0,0,0,255);
    SDL_Rect rectangle;
    rectangle.x=0;
    rectangle.y=0;
    rectangle.w=window_dimensions.w;
    rectangle.h=window_dimensions.h;
    SDL_RenderFillRect(renderer,&rectangle);
}

void afficher_image(SDL_Texture* fond,SDL_Renderer* renderer,SDL_Window *window){
    SDL_Rect window_dimensions;
    SDL_GetWindowSize(
      window, &window_dimensions.w,
      &window_dimensions.h); 
    SDL_Rect rect;
    rect.x = 0;
    rect.y = 0;
    rect.w = window_dimensions.w;
    rect.h = window_dimensions.h;
    SDL_RenderCopy(renderer, fond, NULL, &rect);
}

void afficher_exercice(int lecon,int exercice, TTF_Font * font,SDL_Renderer* renderer,SDL_Window *window){

    SDL_Color color = {100,100,255,255};
    SDL_Surface* text_surface = NULL; 
    char temp[128];
    
    sprintf(temp,"Leçon %d et exercice %d",lecon,exercice);
    text_surface = TTF_RenderUTF8_Blended(font, temp, color);
    if (text_surface == NULL) end_sdl(0, "Can't create text surface", font, window, renderer);

    SDL_Texture* text_texture = NULL;                                    // la texture qui contient le texte
    
    text_texture = SDL_CreateTextureFromSurface(renderer, text_surface); // transfert de la surface à la texture
    if (text_texture == NULL) end_sdl(0, "Can't create texture from surface", font, window, renderer);
    SDL_FreeSurface(text_surface);                                       // la texture ne sert plus à rien

    

    SDL_Rect pos = {0, 0, 0, 0};                                         // rectangle où le texte va être prositionné
    SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h);          // récupération de la taille (w, h) du texte 
    
    pos.x = 10;
    pos.y = 0;
    SDL_RenderCopy(renderer, text_texture, NULL, &pos);                  // Ecriture du texte dans le renderer   
    SDL_DestroyTexture(text_texture);                                    // On n'a plus besoin de la texture avec le texte
}

void afficher_but(char *text, TTF_Font * font,SDL_Renderer* renderer,SDL_Window *window){
    SDL_Color color = {255,255,255,255};
    SDL_Surface* text_surface = NULL; 
    
    SDL_Rect window_dimensions;
    SDL_GetWindowSize(
      window, &window_dimensions.w,
      &window_dimensions.h);   

    char temp[128];
    sprintf(temp,"%s",text);
    text_surface = TTF_RenderUTF8_Blended(font, temp, color);
    if (text_surface == NULL) end_sdl(0, "Can't create text surface", font, window, renderer);

    SDL_Texture* text_texture = NULL;                                    // la texture qui contient le texte

    text_texture = SDL_CreateTextureFromSurface(renderer, text_surface); // transfert de la surface à la texture
    if (text_texture == NULL) end_sdl(0, "Can't create texture from surface", font, window, renderer);
    SDL_FreeSurface(text_surface);                                       // la texture ne sert plus à rien

    

    SDL_Rect pos = {0, 0, 0, 0};                                         // rectangle où le texte va être prositionné
    SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h);          // récupération de la taille (w, h) du texte 
    pos.x = 20;
    pos.y = 60;
    SDL_RenderCopy(renderer, text_texture, NULL, &pos);                  // Ecriture du texte dans le renderer   
    SDL_DestroyTexture(text_texture);                                    // On n'a plus besoin de la texture avec le texte
}

void afficher_nombre(int nombre, float p, TTF_Font * font,SDL_Renderer* renderer,SDL_Window *window){
    SDL_Color color = {0,255,255,255};
    SDL_Surface* text_surface = NULL; 
    
    SDL_Rect window_dimensions;
    SDL_GetWindowSize(
      window, &window_dimensions.w,
      &window_dimensions.h);   

    char temp[128];
    nombre = (int) (nombre - (nombre *p));
    sprintf(temp,"N: %d", nombre);
    text_surface = TTF_RenderUTF8_Blended(font, temp, color);
    if (text_surface == NULL) end_sdl(0, "Can't create text surface", font, window, renderer);

    SDL_Texture* text_texture = NULL;                                    // la texture qui contient le texte

    text_texture = SDL_CreateTextureFromSurface(renderer, text_surface); // transfert de la surface à la texture
    if (text_texture == NULL) end_sdl(0, "Can't create texture from surface", font, window, renderer);
    SDL_FreeSurface(text_surface);                                       // la texture ne sert plus à rien

    

    SDL_Rect pos = {0, 0, 0, 0};                                         // rectangle où le texte va être prositionné
    SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h);          // récupération de la taille (w, h) du texte 
    pos.x = window_dimensions.w/2 -pos.w/2;
    pos.y = 0;
    SDL_RenderCopy(renderer, text_texture, NULL, &pos);                  // Ecriture du texte dans le renderer   
    SDL_DestroyTexture(text_texture);                                    // On n'a plus besoin de la texture avec le texte
}

void afficher_erreur(int erreur, TTF_Font * font,SDL_Renderer* renderer,SDL_Window *window){
    SDL_Color red = {255,0,0,255};
    SDL_Color green = {0,255,0,255};
    SDL_Color color;
    if (erreur > NOMBRE_ERREUR) {
        color = red;
    } else {
        color = green;
    }
    SDL_Surface* text_surface = NULL; 
    
    SDL_Rect window_dimensions;
    SDL_GetWindowSize(
      window, &window_dimensions.w,
      &window_dimensions.h);

    char temp[128];
    sprintf(temp,"Erreur : %d",erreur);
    text_surface = TTF_RenderUTF8_Blended(font, temp, color);
    if (text_surface == NULL) end_sdl(0, "Can't create text surface", font, window, renderer);

    SDL_Texture* text_texture = NULL;                                    // la texture qui contient le texte

    text_texture = SDL_CreateTextureFromSurface(renderer, text_surface); // transfert de la surface à la texture
    if (text_texture == NULL) end_sdl(0, "Can't create texture from surface", font, window, renderer);
    SDL_FreeSurface(text_surface);                                       // la texture ne sert plus à rien

    

    SDL_Rect pos = {0, 0, 0, 0};                                         // rectangle où le texte va être prositionné
    SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h);          // récupération de la taille (w, h) du texte 
    pos.x = window_dimensions.w - pos.w -20;
    pos.y = 0;
    SDL_RenderCopy(renderer, text_texture, NULL, &pos);                  // Ecriture du texte dans le renderer   
    SDL_DestroyTexture(text_texture);                                    // On n'a plus besoin de la texture avec le texte
}

void afficher_temps(float temps, TTF_Font * font,SDL_Renderer* renderer,SDL_Window *window){
    SDL_Color color = {0,255,255,255};
    SDL_Surface* text_surface = NULL; 
    
    SDL_Rect window_dimensions;
    SDL_GetWindowSize(
      window, &window_dimensions.w,
      &window_dimensions.h);

    char temp[128];
    sprintf(temp,"Temps : %0.3f s",temps);
    text_surface = TTF_RenderUTF8_Blended(font, temp, color);
    if (text_surface == NULL) end_sdl(0, "Can't create text surface", font, window, renderer);

    SDL_Texture* text_texture = NULL;                                    // la texture qui contient le texte

    text_texture = SDL_CreateTextureFromSurface(renderer, text_surface); // transfert de la surface à la texture
    if (text_texture == NULL) end_sdl(0, "Can't create texture from surface", font, window, renderer);
    SDL_FreeSurface(text_surface);                                       // la texture ne sert plus à rien

    

    SDL_Rect pos = {0, 0, 0, 0};                                         // rectangle où le texte va être prositionné
    SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h);          // récupération de la taille (w, h) du texte 
    pos.x = window_dimensions.w - pos.w -20;
    pos.y = window_dimensions.h -pos.h -20;
    SDL_RenderCopy(renderer, text_texture, NULL, &pos);                  // Ecriture du texte dans le renderer   
    SDL_DestroyTexture(text_texture);                                    // On n'a plus besoin de la texture avec le texte
}

void afficher_text(char *text,char *resultat, TTF_Font * font, SDL_Renderer* renderer,SDL_Window *window){
    SDL_Color red = {255,0,0,255};
    SDL_Color green = {0,255,0,255};
    SDL_Color color;
    int n = strncmp(text,resultat,strlen(text));
    //printf("Voici le test :%d\n",n);
    if (n) {
        color = red;
    } else {
        color = green;
    }
    
    SDL_Surface* text_surface = NULL; 
    
    SDL_Rect window_dimensions;
    SDL_GetWindowSize(
      window, &window_dimensions.w,
      &window_dimensions.h);   

    // sprintf(coins_get,"pieces obtenues : %d / %d",(score/10),NBR_COINS);
    // text_surface1 = TTF_RenderText_Blended(font, coins_get, color); // création du texte dans la surface 
    // if (text_surface1 == NULL) end_sdl(0, "Can't create text surface", font, window, renderer);

    if (text[0] != '\0') {
        text_surface = TTF_RenderUTF8_Blended(font, text, color);
        if (text_surface == NULL) end_sdl(0, "Can't create text surface", font, window, renderer);

        SDL_Texture* text_texture = NULL;                                    // la texture qui contient le texte

        text_texture = SDL_CreateTextureFromSurface(renderer, text_surface); // transfert de la surface à la texture
        if (text_texture == NULL) end_sdl(0, "Can't create texture from surface", font, window, renderer);
        SDL_FreeSurface(text_surface);                                       // la texture ne sert plus à rien

        

        SDL_Rect pos = {0, 0, 0, 0};                                         // rectangle où le texte va être prositionné
        SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h);          // récupération de la taille (w, h) du texte 
        // pos.x = 0.05*window_dimensions.w;
        // pos.y = 0.05*window_dimensions.h;
        pos.x = 20;
        pos.y = 120;
        if (pos.w > window_dimensions.w-2*pos.x) printf("trop grand\n");
        SDL_RenderCopy(renderer, text_texture, NULL, &pos);                  // Ecriture du texte dans le renderer   
        SDL_DestroyTexture(text_texture);                                    // On n'a plus besoin de la texture avec le texte
    }
}

void afficher_text2(char *text,char *resultat, TTF_Font * font, SDL_Renderer* renderer,SDL_Window *window){
    SDL_Color red = {255,0,0,255};
    SDL_Color green = {0,255,0,255};
    SDL_Color color;
    color = green;
    if (strncmp(text,resultat,strlen(text))){
        color = red;
    } else {
        color = green;
    }
    SDL_Surface* text_surface = NULL; 
    
    SDL_Rect window_dimensions;
    SDL_GetWindowSize(
      window, &window_dimensions.w,
      &window_dimensions.h);   

    char buffer[TAILLE_BUFFER];
    int n = strlen(text);
    int nresultat = strlen(resultat);
    int j = 0;
    int i = 0;
    int ligne =0;
    int position = 0;
    SDL_Texture* text_texture = NULL; 
    while (i+j<n && n < nresultat) {//deuxieme condition pour pasde boucle infini
        reset(buffer);
        while(i+j<n && text[i+j] != ' ' && text[i+j] != '\0') {
            j++;
            
        }
        //printf("fuuu %d %d %d %d\n",i,j,position,ligne);
        strncpy(buffer,text+position,i+j-position);
        text_surface = TTF_RenderUTF8_Blended(font, buffer, color);
        if (text_surface == NULL) end_sdl(0, "Can't create text surface", font, window, renderer);
                                           // la texture qui contient le texte
        text_texture = SDL_CreateTextureFromSurface(renderer, text_surface); // transfert de la surface à la texture
        if (text_texture == NULL) end_sdl(0, "Can't create texture from surface", font, window, renderer);
        SDL_FreeSurface(text_surface);                                       // la texture ne sert plus à rien
        SDL_Rect pos = {0, 0, 0, 0};                                         // rectangle où le texte va être prositionné
        SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h);          // récupération de la taille (w, h) du texte 
        // pos.x = 0.05*window_dimensions.w;
        // pos.y = 0.05*window_dimensions.h;
        pos.x = 20;
        pos.y = 120 + ligne*60;
        if (pos.w <= window_dimensions.w-2*pos.x) {
            SDL_RenderCopy(renderer, text_texture, NULL, &pos);// Ecriture du texte dans le renderer
            i = i+j+1;
            j = 0;
        } else {
            position = i;
            ligne ++;
            j=0;
        }
        
        SDL_DestroyTexture(text_texture);                                    // On n'a plus besoin de la texture avec le texte
    }
    

    if (0 && text[0] != '\0') {
        text_surface = TTF_RenderUTF8_Blended(font, text, color);
        if (text_surface == NULL) end_sdl(0, "Can't create text surface", font, window, renderer);

        SDL_Texture* text_texture = NULL;                                    // la texture qui contient le texte

        text_texture = SDL_CreateTextureFromSurface(renderer, text_surface); // transfert de la surface à la texture
        if (text_texture == NULL) end_sdl(0, "Can't create texture from surface", font, window, renderer);
        SDL_FreeSurface(text_surface);                                       // la texture ne sert plus à rien

        

        SDL_Rect pos = {0, 0, 0, 0};                                         // rectangle où le texte va être prositionné
        SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h);          // récupération de la taille (w, h) du texte 
        // pos.x = 0.05*window_dimensions.w;
        // pos.y = 0.05*window_dimensions.h;
        pos.x = 20;
        pos.y = 120;
        if (pos.w > window_dimensions.w-2*pos.x) printf("trop grand\n");
        SDL_RenderCopy(renderer, text_texture, NULL, &pos);                  // Ecriture du texte dans le renderer   
        SDL_DestroyTexture(text_texture);                                    // On n'a plus besoin de la texture avec le texte
    }
}

int isalpha_cor(int c){//si on met char ca bug car char plus petit que int
    return ((c>='a') && (c<='z'));
}

int main(){

    int largeur =  1200; //1440
    int hauteur = 800;
    SDL_Window *window = NULL;               // Future fenêtre
    /* Initialisation de la SDL  + gestion de l'échec possible */
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
    SDL_Log("Error : SDL initialisation - %s\n", SDL_GetError());      // l'initialisation de la SDL a échoué 
    exit(EXIT_FAILURE);
    }

    /* Création de la fenêtre */
    window = SDL_CreateWindow(
        "Apprend a taper",                    // codage en utf8, donc accents possibles
        SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,                                  // coin haut gauche en haut gauche de l'écran
        largeur, hauteur,                              // largeur , hauteur
        SDL_WINDOW_RESIZABLE);                 // redimensionnable
    if (window == NULL) {
    SDL_Log("Error : SDL window 1 creation - %s\n", SDL_GetError());   // échec de la création de la fenêtre
    SDL_Quit();
    exit(EXIT_FAILURE);
    }

    if (TTF_Init() != 0){
    fprintf(stderr, "Erreur d'initialisation TTF : %s\n", TTF_GetError()); 
    }

    TTF_Font * font = NULL;

    font = TTF_OpenFont("../res/font/arial/arial_unicode_ms/ArialUnicodeMS.ttf", 40 );
    //TTF_SetFontStyle(font, TTF_STYLE_ITALIC | TTF_STYLE_BOLD);           // en italique, gras


    int flags=IMG_INIT_JPG|IMG_INIT_PNG;
    int initted= 0;

    initted = IMG_Init(flags);

    if((initted&flags) != flags) 
    {
        printf("IMG_Init: Impossible d'initialiser le support des formats JPG et PNG requis!\n");
        printf("IMG_Init: %s\n", IMG_GetError());
    }
    
    //------------------------------------------------------------------
    /* Création du renderer */
    SDL_Renderer* renderer = NULL;
    renderer = SDL_CreateRenderer(
            window, -1, 0);
    if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", NULL, window, renderer);

    SDL_Texture * fond = IMG_LoadTexture(renderer,"pos_hands.jpeg");
    if (fond == NULL) end_sdl(0, "Echec du chargement de l'image dans la texture", font, window, renderer);

    SDL_bool lshift = SDL_FALSE;
    SDL_bool rshift = SDL_FALSE;
    SDL_bool lalt = SDL_FALSE;
    SDL_bool ralt = SDL_FALSE;
    SDL_bool chapeau = SDL_FALSE;
    SDL_bool deuxpoints = SDL_FALSE;
    int lecon = LECON;
    int exercice = EXERCICE;
    int erreur = 0;
    char buffer[TAILLE_BUFFER];
    reset(buffer);
    int indice = 0;
    char * lecon0[] = {"qsdf mlkj","fdsq jklm","qmfj sdlk","qdmk sflj","slfj qmkd","flsm qkdl","qdfj ksml","lqfk jsmd"};
    char * lecon1[] = {"fgjh","gfhj","hgjf","fhjg","qmgh","sglh","dgkh","gqhm"};
    char * lecon2[] = {"qamp","aqpm","pas mal","mama papa","appas pampa","gala alpha","jappa passa","alpaga hammam"};
    char * lecon3[] = {"szlo","zsol","fdza","jkop","zoo jazz","gaz soja","polka zozo","zaza slalom"};
    char * lecon4[] = {"deki","edik","demi sommeil","geai pie","mille liasses","solide falaise","pagaille pillage","la demoiselle filme sa famille"};
    char * lecon5[] = {"frju","rfuj","ours rieur","fleur fragile","lourd fardeau","salaire de mars","parure fourrure","arrosez les fleurs sur le mur puis les radis puis les salades"};
    char * lecon6[] = {"frft","jujy","fgft","jhjy","stylo type","rythme et yoga","yaourt et yogourt","il fait des efforts pour les rattraper mais tout espoir est perdu"};
    char * lecon7[] = {"qwsx","wqxs","deux kiwis","prix du kilowatt","taxi et tramway","prix de deux whiskies","texte approximatif","elle aime les edelweiss les vieux cailloux les oiseaux exotiques"};
    char * lecon8[] = {"dcfv","cdvf","vice et versa","vieux cycle","victoire du coureur","clercs et avocats","acceuil du visiteur","il a vu le cheval de course victorieux arriver vers lui"};
    char * lecon9[] = {"fvfb","jhjn","bonne banque","ballon blanc","cabanon habitation","avantage abonnement","fonctionnement convenable","toutes nos bicyclettes sont garanties contre tout vice de fabrication"};
    char * lecon10[] = {"qaqé","jujè","bébé","père et mère","répète élève","phénomène légèreté","éphémères ténèbres","le succès de la pièce était assuré mais le résultat a été décevant"};
    char * lecon11[] = {"kikç","lolà","voilà façade","balançoire déjà","voilà le garçon","çà et là","français à pied","le commerçant a déjà réglé le maçon qui lui a donné un reçu"};
    char * lecon12[] = {"ftf-","mùm","même où la main-d'œuvre","là-haut où vous avez pêle-mêle","c'est-à-dire à côté du rez-de-chaussée","là où vous êtes allés dîner en vis-à-vis","il y a une fôret là où on trouve le hêtre le chêne et le frêne","à cette même date elle travaillera à mi-temps par demi-journées"};
    char * lecon13[] = {"j,k;","l:m!","virgule,","point et virgule ;","deux points :","deux espaces !","il avait nettoyé, lavé, frotté, rangé ; à présent il était prêt ;","premier principe : bien lire le sujet !"};
    char * lecon14[] = {"szs\"","ded'","c'est \"urgent\"","c'est ce qu'il a \"vu\" ;","\"à la française\" ; \"à l'italienne\" ;","parce qu'il n'est pas \"né d'aujourd'hui\" ! c'est sûr !","n'oubliez pas d'écrire \"en toutes lettres\"","c'est \"personnel\" et \"confidentiel\" : je l'ai déjà noté !"};
    char * lecon15[] = {"frf(","mpm)","(au choix) (par avion)","(à l'essai) (par chèque)","(à l'américaine) (demi-gros)","en tête-à-tête) (en vis-à-vis)","veuillez me communiquer au plus tôt vos conditions (prix et délais)","tous les enfants (petits et grands) sont invités (le samedi)"};
    char * lecon16[] = {"Madame Alice MARTIN","j?k.l/","Moret/Loing Seine-et-Marne (Ile-de-France)","Qui ? Lui. Sans doute...","Où ? Aujourd'hui. C'est la vérité...","Pourquoi ? C'est d'un bon rapport qualité/prix. C'est vrai...","Par où êtes-vous entré ? Depuis quand êtes-vous là ? Où allez-vous à présent ?","Il n'est pas allé à Cagnes/Mer. D'ailleurs, il n'a rien dit."};
    char * lecon17[] = {"m¨m","m\%m","m\%m m°m","maïs faïence aiguë ;","escompte : 5\% ; alcool à 90° ;","Moïse, Saint-Raphaël ;","A Noël, je rêve d'aller aux Caraïbes, ou en Israël, ou à Haïti.","Il a obtenu une remise de 10\% sur la facture n°125."};
    char ** text[] = {lecon0,lecon1,lecon2,lecon3,lecon4,lecon5,lecon6,lecon7,lecon8,lecon9,lecon10,lecon11,lecon12,lecon13,lecon14,lecon15,lecon16,lecon17};
    int nombre_text[][8] = {{12,12,12,12,12,12,12,12},
                            {24,24,24,24,24,24,24,16},
                            {18,18,14,10,8,12,10,8},
                            {26,26,26,26,14,14,10,10},
                            {22,22,8,10,10,10,8,4},
                            {26,26,12,10,8,8,8,2},
                            {28,28,28,28,12,8,6,4},
                            {20,20,10,8,8,6,6,2},
                            {12,12,10,10,6,8,6,4},
                            {24,24,8,10,6,6,4,4},
                            {20,26,10,10,10,6,6,4},
                            {24,26,10,8,8,16,8,2},
                            {30,20,4,4,4,4,2,4},
                            {30,24,14,8,10,8,2,4},
                            {26,22,8,6,4,4,4,2},
                            {30,18,6,6,6,4,4,2},
                            {4,22,4,6,4,4,4,2},
                            {22,16,10,6,4,6,2,2}};
    if (NOMBRE != 0){
        for(int i = 0;i<18;++i){
            for(int j =0;j<8;++j){
                nombre_text[i][j] = NOMBRE;
            }
        }
    }

    //printf("Voici le truc %d\n",nombre_text[0][0]);
    char resultat[TAILLE_BUFFER];
    assigne(resultat,text,exercice,lecon,nombre_text);
    // printf("buff %s\n",buffer);
    // printf("resultat %s\n",resultat);

    //char test[]="œ";
    //printf("%d %d\n",test[0],test[1]);
    struct timeval start;
    struct timeval end;
    gettimeofday(&start, NULL);
    SDL_bool program_on = SDL_TRUE;
    int boucle_img;
    printf("\nCommencement du programm\n");
    while (program_on) {                              // La boucle des évènements
        SDL_Event event;                                // Evènement à traiter

        while (program_on && SDL_PollEvent(&event)) {   // Tant que la file des évènements stockés n'est pas vide et qu'on n'a pas
                                                        // terminé le programme Défiler l'élément en tête de file dans 'event'
            if (buffer[0] == '\0') gettimeofday(&start, NULL);
            if ((event.type==SDL_KEYDOWN) && (event.key.keysym.sym==SDLK_LSHIFT)) lshift = SDL_TRUE;
            if ((event.type==SDL_KEYDOWN) && (event.key.keysym.sym==SDLK_RSHIFT)) rshift = SDL_TRUE;
            if ((event.type==SDL_KEYUP) && (event.key.keysym.sym==SDLK_LSHIFT)) lshift = SDL_FALSE;
            if ((event.type==SDL_KEYUP) && (event.key.keysym.sym==SDLK_RSHIFT)) rshift = SDL_FALSE;
            if ((event.type==SDL_KEYDOWN) && (event.key.keysym.sym==SDLK_LALT)) lalt = SDL_TRUE;
            if ((event.type==SDL_KEYDOWN) && (event.key.keysym.sym==SDLK_RALT)) ralt = SDL_TRUE;
            if ((event.type==SDL_KEYUP) && (event.key.keysym.sym==SDLK_LALT)) lalt = SDL_FALSE;
            if ((event.type==SDL_KEYUP) && (event.key.keysym.sym==SDLK_RALT)) ralt = SDL_FALSE;
            
            switch (event.type) {                         // En fonction de la valeur du type de cet évènement
                case SDL_QUIT:                           // Un évènement simple, on a cliqué sur la x de la // fenêtre
                    program_on = SDL_FALSE;                     // Il est temps d'arrêter le programme
                    break;
                case SDL_KEYDOWN:
                    // printf("l:%d\n",event.key.keysym.sym);
                    if (isdigit(event.key.keysym.sym) && (lshift || rshift)) {
                        buffer[indice++] = event.key.keysym.sym;
                        test_erreur(&erreur,resultat,buffer);
                    } else {
                        if (event.key.keysym.sym == SDLK_o && (lalt || ralt)){
                            buffer[indice++] = -59;
                            buffer[indice++] = -109;
                            test_erreur(&erreur,resultat,buffer);
                            break;
                        } else {
                            if  (isalpha_cor(event.key.keysym.sym)){
                                if (chapeau) {
                                    chapeau = SDL_FALSE;
                                    switch (event.key.keysym.sym) {
                                        case 'a':
                                            buffer[indice-1] = -61;
                                            buffer[indice++] = -94;
                                            test_erreur(&erreur,resultat,buffer);
                                            break;
                                        case 'e':
                                            buffer[indice-1] = -61;
                                            buffer[indice++] = -86;
                                            test_erreur(&erreur,resultat,buffer);
                                            break;
                                        case 'i':
                                            buffer[indice-1] = -61;
                                            buffer[indice++] = -82;
                                            test_erreur(&erreur,resultat,buffer);
                                            break;
                                        case 'o':
                                            buffer[indice-1] = -61;
                                            buffer[indice++] = -76;
                                            test_erreur(&erreur,resultat,buffer);
                                            break;
                                        case 'u':
                                            buffer[indice-1] = -61;
                                            buffer[indice++] = -69;
                                            test_erreur(&erreur,resultat,buffer);
                                            break;
                                    }
                                } else {
                                    if (deuxpoints){
                                        deuxpoints = SDL_FALSE;
                                        switch (event.key.keysym.sym) {
                                            case 'a':
                                                buffer[indice-2] = -61;
                                                buffer[indice-1] = -92;
                                                test_erreur(&erreur,resultat,buffer);
                                                break;
                                            case 'e':
                                                buffer[indice-2] = -61;
                                                buffer[indice-1] = -85;
                                                test_erreur(&erreur,resultat,buffer);
                                                break;
                                            case 'i':
                                                buffer[indice-2] = -61;
                                                buffer[indice-1] = -81;
                                                test_erreur(&erreur,resultat,buffer);
                                                break;
                                            case 'o':
                                                buffer[indice-2] = -61;
                                                buffer[indice-1] = -74;
                                                test_erreur(&erreur,resultat,buffer);
                                                break;
                                            case 'u':
                                                buffer[indice-2] = -61;
                                                buffer[indice-1] = -68;
                                                test_erreur(&erreur,resultat,buffer);
                                                break;
                                        }
                                    } else {
                                        buffer[indice] = event.key.keysym.sym;
                                        if (lshift || rshift) buffer[indice] += 'A' - 'a';
                                        indice++;
                                        test_erreur(&erreur,resultat,buffer);
                                    }
                                }
                            } else {
                                switch (event.key.keysym.sym) {             // la touche appuyée est ...
                                    case SDLK_SPACE:
                                        buffer[indice++] = SDLK_SPACE;
                                        test_erreur(&erreur,resultat,buffer);
                                        break;
                                    case SDLK_BACKSPACE:
                                        chapeau = SDL_FALSE;
                                        deuxpoints = SDL_FALSE;
                                        do {
                                            if (indice>0) indice--;
                                            if (buffer[indice]<0){ //caractere spécial
                                                buffer[indice--] = '\0';
                                            }
                                            buffer[indice] = '\0';
                                        } while (strncmp(buffer,resultat,strlen(buffer)));
                                        break;
                                    case SDLK_2://é
                                        buffer[indice++] = -61;
                                        buffer[indice++] = -87;
                                        test_erreur(&erreur,resultat,buffer);
                                        break;
                                    case SDLK_3://"
                                        buffer[indice++] = '"';
                                        test_erreur(&erreur,resultat,buffer);
                                        break;
                                    case SDLK_4://'
                                        buffer[indice++] = 39;
                                        test_erreur(&erreur,resultat,buffer);
                                        break;
                                    case SDLK_5://(
                                        buffer[indice++] = '(';
                                        test_erreur(&erreur,resultat,buffer);
                                        break;
                                    case SDLK_6://-
                                        buffer[indice++] = '-';
                                        test_erreur(&erreur,resultat,buffer);
                                        break;
                                    case SDLK_7://è
                                        buffer[indice++] = -61;
                                        buffer[indice++] = -88;
                                        test_erreur(&erreur,resultat,buffer);
                                        break;
                                    case SDLK_9://ç
                                        buffer[indice++] = -61;
                                        buffer[indice++] = -89;
                                        test_erreur(&erreur,resultat,buffer);
                                        break;
                                    case SDLK_0://à
                                        buffer[indice++] = -61;
                                        buffer[indice++] = -96;
                                        test_erreur(&erreur,resultat,buffer);
                                        break;
                                    case 41://) °
                                        if (lshift || rshift) {
                                            buffer[indice++] = -62;
                                            buffer[indice++] = -80;
                                        } else {
                                            buffer[indice++] = ')';
                                        }
                                        test_erreur(&erreur,resultat,buffer);
                                        break;
                                    case 249://ù
                                        if (lshift || rshift) {
                                            
                                            buffer[indice++] = '%';
                                        } else {
                                            buffer[indice++] = -61;
                                            buffer[indice++] = -71;
                                        }
                                        test_erreur(&erreur,resultat,buffer);
                                        break;
                                    case ','://, ?
                                        if (lshift || rshift) {
                                            buffer[indice++] = '?';
                                        } else {
                                            buffer[indice++] = ',';
                                        }
                                        test_erreur(&erreur,resultat,buffer);
                                        break;
                                    case ';'://; .
                                        if (lshift || rshift) {
                                            buffer[indice++] = '.';
                                        } else {
                                            buffer[indice++] = ';';
                                        }
                                        test_erreur(&erreur,resultat,buffer);
                                        break;
                                    case ':'://: /
                                        if (lshift || rshift) {
                                            buffer[indice++] = '/';
                                        } else {
                                            buffer[indice++] = ':';
                                        }
                                        test_erreur(&erreur,resultat,buffer);
                                        break;
                                    case 61://'!'
                                        buffer[indice++] = '!';
                                        test_erreur(&erreur,resultat,buffer);
                                        break;
                                    case '^'://^ ¨
                                        if (lshift || rshift) {
                                            deuxpoints = SDL_TRUE;
                                            buffer[indice++] = -62;
                                            buffer[indice++] = -88;
                                        } else {
                                            chapeau = SDL_TRUE;
                                            buffer[indice++] = '^';
                                        }
                                        break;
                                    case SDLK_UP:
                                        erreur = 0;
                                        exercice++;
                                        if (exercice >= 8){
                                            exercice = 0;
                                            lecon++;
                                        }
                                        reset(buffer);
                                        indice = 0;
                                        assigne(resultat,text,exercice,lecon,nombre_text);
                                        gettimeofday(&start, NULL);
                                        break;
                                    case SDLK_DOWN:
                                        erreur = 0;
                                        exercice--;
                                        if (exercice <0){
                                            exercice = 7;
                                            lecon--;
                                        }
                                        reset(buffer);
                                        indice = 0;
                                        assigne(resultat,text,exercice,lecon,nombre_text);
                                        gettimeofday(&start, NULL);
                                        break;
                                    case SDLK_RIGHT:
                                        erreur = 0;
                                        lecon++;
                                        reset(buffer);
                                        indice = 0;
                                        assigne(resultat,text,exercice,lecon,nombre_text);
                                        gettimeofday(&start, NULL);
                                        break;
                                    case SDLK_LEFT:
                                        erreur = 0;
                                        lecon--;
                                        reset(buffer);
                                        indice = 0;
                                        assigne(resultat,text,exercice,lecon,nombre_text);
                                        break;
                                    case SDLK_ESCAPE:
                                        SDL_FlushEvent(SDL_KEYDOWN);//flush
                                        afficher_image(fond,renderer,window);
                                        afficherEcran(renderer);
                                        SDL_Delay(100);
                                        boucle_img = 1;
                                        while (program_on && boucle_img){
                                            while (program_on && boucle_img && SDL_PollEvent(&event)){
                                                boucle_img = !(event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE);
                                                if (event.type == SDL_QUIT) program_on = SDL_FALSE;
                                                SDL_Delay(100);
                                            }
                                        }
                                        break;
                                    case SDLK_RETURN:
                                        reset(buffer);
                                        erreur = 0;
                                        indice = 0;
                                        gettimeofday(&start, NULL);
                                        break;
                                }
                            }
                        }
                    } 
                    break;
            }
            gettimeofday(&end, NULL);
            afficher_fond(renderer,window);
            afficher_exercice(lecon,exercice,font,renderer,window);
            afficher_but(text[lecon][exercice],font,renderer,window);
            afficher_nombre(nombre_text[lecon][exercice],strlen(buffer)/ (float) strlen(resultat),font,renderer,window);
            afficher_erreur(erreur,font,renderer,window);
            afficher_temps(time_diff(&start, &end),font,renderer,window);
            afficher_text2(buffer,resultat,font,renderer,window);
            afficherEcran(renderer);
            SDL_Delay(16);// Petite pause

            if (strncmp(buffer,resultat,strlen(buffer))==0 && strlen(buffer)== strlen(resultat)){//fin de la sequence
                printf("Temps %0.3f\n",time_diff(&start, &end));
                printf("Nombre de mots %0.1f par minutes\n",(strcomptocc(buffer,' ') / time_diff(&start, &end))*60);
                printf("Nombre de caractères %0.1f par minutes\n",(strlen(buffer) / time_diff(&start, &end))*60);
                printf("Nombre d'erreurs : %d\n",erreur);
                if (erreur <= NOMBRE_ERREUR){
                    exercice++;
                    if (exercice >= 8){
                        exercice = 0;
                        lecon++;
                    }
                }
                reset(buffer);
                indice = 0;
                erreur = 0;
                assigne(resultat,text,exercice,lecon,nombre_text);
                gettimeofday(&start, NULL);
                
            }
        }
    }
    // printf("Taille %d\n",lenliste(malistedemots));
    // freeliste(&malistedemots);
    // malistedemots = NULL;
    // printf("FreeListe\n");
    end_sdl(0, "Normal ending", font, window, renderer);//renderer
    return EXIT_SUCCESS;
}